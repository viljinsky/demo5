/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.viljinsky.address;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Разбор даных из файла для базы ADDRESS
 * @author viljinsky
 */
public abstract class AddressParser extends Recordset {

    /**
     * Имя файла для разбора
     */
    String fileName;
    /**
     * Максимально кол записей (меньше 0 - все)
     */
    Integer max;
    /**
     * Признак что первая строка - заголовки (исключается)
     */
    boolean firstRowIsHeader = false;

    public AddressParser(Recordset recordset, String fileName) {
        this(recordset, fileName, -1);
    }

    public AddressParser(Recordset recordset, String fileName, Integer max) {
        super(recordset);
        this.max = max;
        this.fileName = fileName;
    }

    public void read() throws Exception {
        File file = new File(fileName);

        try (FileInputStream in = new FileInputStream(file);
                InputStreamReader reader = new InputStreamReader(in, "utf-8");
                BufferedReader br = new BufferedReader(reader);) {

            Progress progress = new Progress("Чтение данных");
            Integer n = 0;

            String line;
            while ((line = br.readLine()) != null) {

                Object[] p = parse(line);
                if (p != null) {
                    add(p);
                    n++;
                }
                if (max > 0 && n > max) {
                    break;
                }
                progress.next();
            }
            if (firstRowIsHeader) {
                remove(0);
            }
            progress.stop();
            if (isEmpty()) {
                throw new RuntimeException("В файле \"" + fileName + "\" нет строк соответствующих шаблону");
            }
        }

    }

    ;
    
    protected abstract Object[] parse(String line);

}

/**
 * Класс чтения данных для формата XML(упрощённый)
 *
 * @author viljinsky
 */
class AddressXML extends AddressParser {

    private static final String s1 = "city=\"([\\S|\\s]+?)\"";
    private static final String s2 = "street=\"([\\S|\\s]+?)\"";
    private static final String s3 = "house=\"(\\d+)\"";
    private static final String s4 = "floor=\"(\\d+)\"";
    private static final String regExp = "<item\\s+" + s1 + "\\s+" + s2 + "\\s+" + s3 + "\\s+" + s4 + "\\s*/>";
    private static Pattern pattern = Pattern.compile(regExp);

    @Override
    public Object[] parse(String line) {
        Matcher m = pattern.matcher(line);
        if (m.find()) {
            return new Object[]{m.group(1), m.group(2), Integer.valueOf(m.group(3)), Integer.valueOf(m.group(4))};
        }
        return null;
    }

    public AddressXML(Recordset recordset, String fileName) {
        super(recordset, fileName);
    }

    public AddressXML(Recordset recordset, String fileName, Integer max) {
        super(recordset, fileName, max);
    }

}

/**
 * Класс чтения данных для формата CSV
 * 
 * @author viljinsky
 */
class AddressCSV extends AddressParser {

    static final String regExp = "(\"?)([^\"?]+)";
    static final String H = "\"";
    static final Pattern pattern = Pattern.compile(regExp);

    Object treem(Object value) {

        Matcher matcher = pattern.matcher(String.valueOf(value));
        if (matcher.find()) {
            if (!matcher.group(1).equals(H)) {
                return Integer.valueOf(matcher.group(2));
            }
            return matcher.group(2);
        }
        return null;

    }

    @Override
    public Object[] parse(String line) {
        String[] fields = line.split(";");
        if (fields.length != 4) {
            return null;
        }
        Object[] result = new Object[4];
        result[0] = treem(fields[0]);
        result[1] = treem(fields[1]);
        result[2] = treem(fields[2]);
        result[3] = treem(fields[3]);
        return result;
    }

    public AddressCSV(Recordset recordset, String fileName) {
        super(recordset, fileName);
        firstRowIsHeader = true;
    }

    public AddressCSV(Recordset recordset, String fileName, Integer max) {
        super(recordset, fileName, max);
        firstRowIsHeader = true;
    }

}
